bertha-build-tools
==================

A Docker image for [Genomics England](https://www.genomicsengland.co.uk/) `bertha` build environment.

These build tools install [python3](https://www.python.org/) and build scripts onto an [alpine](https://alpinelinux.org/) [docker image](https://hub.docker.com/_/alpine).


Local Setup
-----------

```bash
    pip3 install git+https://gitlab.com/genomicsengland/opensource/bertha-build-tools.git@master
```

Testing Locally
---------------

```bash
    # navigate to a project folder - shell into build context
    docker run --rm -it -v /var/run/docker.sock:/var/run/docker.sock --volume ${PWD}:${PWD} --workdir ${PWD} registry.gitlab.com/genomicsengland/opensource/bertha-build-tools:latest /bin/sh
```
