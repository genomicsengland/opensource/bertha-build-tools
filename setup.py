from os import path
from setuptools import setup, find_packages

_here = path.dirname(path.abspath(__file__))

setup(
    name='bertha-build-tools',
    description='',
    version='0.0.1',
    packages=find_packages(
        where='.',
        exclude=["*.tests", "*.tests.*", "tests.*", "tests"],
    ),
    include_package_data=True,
    #long_description=open(path.join(_here, 'README.md')).read(),
    install_requires=[
    ],
    url='https://gitlab.com/genomicsengland/opensource/bertha-build-tools',
    author='Genomics England',
    author_email='',
    license='MIT',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Topic :: Software Development :: Libraries',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ],
    keywords=[
    ],
)
